function [ output_args ] = test( input_args )
%TEST Summary of this function goes here
%   Detailed explanation goes here

L0 = 0.03;

MuW = [L0,L0*0.4,L0*0.8];

alpha = 100;
%SigmaW = [0.000001,0.0001,0.000001];

t = linspace(L0,L0*0.8,100);

h(:,1) = ones(1,100);
for i=2:3
	h(:,i) = ones(1,100) - exp(-alpha*(t-repmat(MuW(i-1),1,100)));
end
for i=1:3
	plot(t,h(:,i));hold on
end

h

pause
close all

end

