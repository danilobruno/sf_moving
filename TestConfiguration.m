function [ P Target ] = TestConfiguration( )
%TESTCONFIGURATION Summary of this function goes here
%   Detailed explanation goes here

q0 = [0 0 0];

q = [ 3.14942e-20 7.26059e-11 0.05 0 0 0.04 0 0 0.04 0 0 0.04];

q = [q0 q];

m = StiffFlop(3);
m.L0 = 0.04;
m.setQ(q);

[Priors,Mu,Sigma] = initGMM(m);

[dq,Target] = rbbcLQR([0,1,2],Priors,Mu,Sigma,m,[1 1 1 1 1],[0;0;0],[0;0;1],0);

P = m.P;

P
Target

end

