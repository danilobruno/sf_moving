node = rosmatlab.node('Matlab','http://localhost:11311');
o = rbbcRos(node,[1,0]);
o.init();

Tip = o.m.P(:,end);
o.StartPos = Tip;	%Starting position of Tip for later algorithm
o.StartOr = o.m.Refs(:,3,end);	%Starting orientation of tip 


