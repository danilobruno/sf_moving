function [dq,Target,point,VV] = rbbcBest(Priors,Mu,Sigma,m,weight)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here


%Modification of rbbc algorithm to move only the point with lower probability

%m if a SiffFlop object

Target = zeros(3,1);

VV = zeros(3,1);

model.Mu = Mu;
model.Sigma = Sigma;
model.Priors = Priors;

modules = m.Links;
discret = 10;
L2 = (m.L0*1.5+2*m.d0);
%point = [2.5,2,1.5,1,0.5,0];

%P1 = zeros(3,length(point));
%for ii = 1:length(point)
%	P1(:,ii) = m.getCoords(point(ii));
%end

LLL = m.getConstCurvCoords(m.q);
SecLength = LLL/m.L0;
[Tip,TotalLength] = m.getCoords(modules);
Ref = m.getTipFrame;
%if (iter > 20) && (iter < nbData - 30)
%	VTip = Tip - Tip0;
%	VTip = Ref'*VTip;
%else
%	VTip = [0;0;1];
%end
in = 1;
out = [2:4];
%Activate attractor if z > 0 (robot inside the body)
dq = zeros(m.nbJVar,1);

zz = zeros(1,modules*discret);
for i=1:modules*discret
	PAll(:,i) = m.getCoords(i/discret);
	zz(i) = PAll(3,i);
end
[~,index] = min(zz<0);
fraction = index/discret;

%sTot = linspace(0,dt*nbData,nbData);
%outTot = GMR(s,1,[2,3],model);
point = 3;
if fraction < 3
	%Pass through trocar port
	[P,LengthTrocar] = m.getCoords(fraction);
	Linside = TotalLength - LengthTrocar;
	V = zeros(3,1);
	J = m.jacobian;
	JJTrocar = m.JJ(fraction);
	if fraction > 0.1 
		V([1,2],1) = -P([1,2],1); %Distance of point from trocar
	else
		V(:,1) = -P(:,1); %Distance of point from trocar
		weigth(2) = 0;
	end
	dq = pinv(JJTrocar)*V;
	%dqTrocar = dq;
	
	%Evaluate probability for all points inside the body
	probPoint = 10000*ones(1,modules*discret);
	for i=modules*discret-1:-1:index+1
		fractionTmp = i/discret;
		PTmp = m.getCoords(fractionTmp);
		probPoint(i) = 0;
		for k=1:size(Sigma,3)
			probPoint(i) = probPoint(i) + Priors(k)*mvnpdf(PTmp,Mu([2:end],k),Sigma([2:end],[2:end],k));
		end
	end
	[~,point] = min(probPoint);
	point = point/discret;
	%%Add other movements in the null space of J and JJTrocar
	%
	%LActivate = [0.5*L2,L2,1.5*L2,2*L2,2.5*L2,3*L2];
	LActivate = (modules-point)*L2;
	for ii = 1:length(point)
		%P10 = P1(:,ii);
		P1(:,ii) = m.getCoords(point(ii));
		%V1(:,ii) = P1(:,ii) - P10;
		%Positions(:,iter,ii) = P1(:,ii);
		if fraction <= point(ii)
			%out = GMR(Tip([1,3],1),[2,4],1,model);
			if Linside < LActivate(ii)
				ll(ii) = 0;
			else 
				ll(ii) = Linside - LActivate(ii);
			end
			out = GMR(ll(ii),1,[2:4],model);

			Tar = zeros(3,1);
			Tar(1,1) = out.Mu(1,1);
			Tar(3,1) = out.Mu(3,1);
			Target(:,ii) = Tar;

			WP = zeros(3);
			WP(1,1) = out.Wp(1,1);
			WP(1,3) = out.Wp(1,3);
			WP(3,1) = out.Wp(3,1);
			WP(3,3) = out.Wp(3,3);
			
			
			prob = gaussPDF(P1([1:3],ii),out.Mu,out.Sigma)/gaussPDF(out.Mu,out.Mu,out.Sigma);
			
			Coeff = 1 - prob;
			%Coeff = 1;
			
			%Proportional controller
			VV = Coeff*(Tar - P1(:,ii)); % - 2*sqrt(Coeff)*V1(:,ii);
			%Critically damped controller
			%VV = Coeff*(Tar - P1(:,ii)); % - 2*sqrt(Coeff)*V1(:,ii);
			
			%ee = m.Refs(:,3,point(ii));
			%VV = (VV - (VV'*ee)*ee);
			
			
			
			%VV(3,1) = 0;
			%VV = VV - (VV'*Vel(:,index+1))*Vel(:,index+1)/(Vel(:,index+1)'*Vel(:,index+1));
			%m.movePoint(point(i),V,10);
			JJ = m.JJ(point(ii));
			%N = null([J;JJTrocar]);
			%W = JJ*N;
			%dq = dq + N*(pinv(W)*VV);
			%
			%dq = dq + (eye(m.nbJVar) - pinv(JJTrocar)*JJTrocar)*pinv(JJ)*VV;
			dq = pinv(JJ)*VV;
			%dq = dq + pinv(JJ)*VV;
			%dq = dq + (eye(m.nbJVar) - pinv(JJ)*JJ)*dqTrocar;
			%dq = zeros(m.nbJVar,1);
			%plot3(Tar(1),Tar(2),Tar(3),'o');
			%quiver3(P1(1),P1(2),P1(3),VV(1),VV(2),VV(3));
			%plot3(P1(1),P1(2),P1(3),'o');
		end
	end

	%TotL = sum(SecLength)/3;
	%dl1 = SecLength(3) - TotL;
	%dl2 = SecLength(3) + SecLength(2)- 2*TotL;

	%VLength = nullLength(m);

	%dq = dq + pinv(m.JJ(2))*dl1*VLength(:,1) + pinv(m.JJ(1))*dl2*VLength(:,2); 

	%dq = dq/(length(point));

	%dq = dq + 0.02*rand(m.nbJVar,1) - 0.01;
	%dq = dqTrocar + 0.02*rand(m.nbJVar,1) - 0.01;




end

end


function [V,dq] = nullLength(m)
%NULLVIEW Summary of this function goes here
%   Detailed explanation goes here

N = null(m.jacobian);
Refs = m.Refs;
dq = zeros(m.nbJVar,1);
for i=1:m.Links-1
	JJ(:,:,i) = m.JJ(i);
	LL = (pinv(JJ(:,:,i))*Refs(:,3,i+1))'*N;
	dqn(:,i) = (LL*N')';
	V(:,i) = JJ(:,:,i)*dqn(:,i);
	V(:,i) = V(:,i) / sqrt(V(:,i)'*V(:,i));
	dq = dq + dqn(:,i);
end

end

