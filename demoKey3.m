function [ output_args ] = mouseData_time( input_args )
%MOUSEDATA Summary of this function goes here
%   Detailed explanation goes here

Time = [];
Data = [];

nbVar = 3;
exitFlag = 0;
scrollCounts = 0;

modules = 2;

m = StiffFlop(modules);
Tip = m.P(:,end);
Start = m.P(:,1);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Define environment
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fig = figure('position',[10 170 550 550]); hold on; box on;
plot3(-0.1,0,-0.1,'.');
plot3(0.1,0,0.1,'.');
view([0 -1 0]);
axis([-0.2 0.2 -0.1 0.1 -0.2 0.3]);
axis manual
axis equal
%Plot manipulator
hhh = m.plot;
%Plot trocar port
ss = linspace(0,2*pi,100);
trocar = [0.03*cos(ss);0.03*sin(ss);zeros(1,100)];
plot3(trocar(1,:),trocar(2,:),trocar(3,:),'Linewidth',3);hold on
%ObstMu = [0.1 -0.1 ;0 0 ;0.15 0.18];
%ObstSigma(:,:,1) = [0.0008 0 0.0006; 0 0.0004 0 ; 0.0006 0 0.001];
%ObstSigma(:,:,2) = [0.004 0 -0.0000; 0 0.0004 0 ; -0.0000 0 0.0004];
%Plot obstacles by using GMM 
ObstMu = [0; 0 ; 0.15];
ObstSigma = [0.0005 0 0 ; 0 0.002 0 ; 0 0 0.0005]; 
%plotGMM3D(ObstMu,ObstSigma,[0.2 0 0],1);

%Cylinder scenario
[xCyl,yCyl,zCyl] = cylinder;

xCylExt = 0.15*xCyl;
yCylExt = 0.15*yCyl;
zCylExt = 0.12*zCyl + 0.15;
xCylInt = 0.06*xCyl;
yCylInt = 0.06*yCyl;
zCylInt = 0.12*zCyl + 0.15;

surf(xCylExt,yCylExt,zCylExt,'FaceColor','red','FaceAlpha',0.3);hold on 
surf(xCylInt,yCylInt,zCylInt,'FaceColor','red','FaceAlpha',1);
ViewPoint = 0;
view(0,ViewPoint);

getData = 0;

set(0,'CurrentFigure',fig);

set(fig,'WindowButtonDownFcn',@winDown);
set(fig,'KeyPressFcn',@readkeys);
setappdata(gcf,'getData',getData);
setappdata(gcf,'exitFlag',exitFlag);
setappdata(gcf,'scrollCounts',scrollCounts);
setappdata(gcf,'Tip',Tip);
setappdata(gcf,'ViewPoint',ViewPoint);

%Initialize DP-Kmeans
lambda = m.L0*1.8;
%lambda = m.L0;
minsigma = lambda/300;

%N = modules;
%Priors = ones(1,N)/N;
%Mu = zeros(4,N);
%Mu(1,:) = m.P(3,[1:end-1]);
%Mu([2:end],:) = m.P(:,[1:end-1]);
%for i=1:N
%	Sigma(:,:,i) = 0.0001*eye(4);
%	Sigma(4,4,i) = 0.001;
%	Sigma(1,4,i) = 0.001;
%	Sigma(1,4,i) = 0.0001;
%	Sigma(4,1,i) = 0.0001;
%end

%Initialize GMM for null space controller
%Sample N points along the central axis of manipulator and build GMM 
%by using EM. Use regularization to make Gaussians wide as manipulator

N = 100;

ss = linspace(0,modules,N);
[~,TotalLength] = m.getCoords(modules); 
for i=1:N
	[PTmp(:,i),LengthTmp(:,i)] = m.getCoords(ss(i));
end
%DataInit(1,:) = - repmat(TotalLength,1,size(TotalLength,2)) + LengthTmp;
DataInit(1,:) = LengthTmp;
DataInit([2:4],:) = PTmp;

[Priors,Mu,Sigma] = EM_init_regularTiming(DataInit,modules,m.d0/100);
[Priors,Mu,Sigma] = EM(DataInit,Priors,Mu,Sigma,m.d0/100);
%plotGMM3D(Mu([2:4],:),Sigma([2:4],[2:4],:),[1 0 0],0.3);

hhh1 = [];
hhh2 = [];
Ref = m.Refs(:,:,end);
P_old = 0;
go = 0;
step = 0.002;

setappdata(gcf,'step',step);
%t = toc;
%point = [2.5,1.5,0.5];
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Set points that aer used by null space controller
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Can be any number of points (Test different possibilities)
point = [1,0];

for ii = 1:length(point)
	P1(:,ii) = m.getCoords(point(ii));
end

while exitFlag == 0
	%figure(fig)
	go = 0;
	%while getData == 1
	figure(fig); %hold off;
	ViewPoint = getappdata(gcf,'ViewPoint');
	view(3,ViewPoint);
	%axis([-0.2 0.2 -0.1 0.3 -0.1 0.3]);
	%view([0 -1 0]);
	%point = get(gca,'CurrentPoint');
	setappdata(gcf,'Ref',Ref);
	getData = getappdata(gcf,'getData');
	Tip = getappdata(gcf,'Tip');
	scrollCounts = getappdata(gcf,'scrollCounts');
	theta = scrollCounts*0.1;
	direction = [sin(theta),0,cos(theta)]';
	setappdata(gcf,'scrollCounts',0);
	%m.translateOf(Tip-m.P(:,end),[1 1 1 1 1],10);
	%m.rotateTo([sin(theta),0,cos(theta)]',[1 1 1 1 1],1);
	nullMotion = zeros(m.nbJVar,1);
	%%Evaluate best null motion with rbbc
	%
	%Trocar port control
	%
	modules = m.Links;
	%discret= 10;
	%zz = zeros(1,modules*discret);
	%for i=1:modules*discret
	%	PPP = m.getCoords(i/discret);
	%	zz(i) = PPP(3);
	%end
	%[~,index] = min(zz<0);
	%zzTmp = abs(zz);
	%[~,index] = min(zzTmp); %index of closest value
	%fraction = index/discret;
	%
	weight = [1 1];
	weight = [weight ones(1,modules)];
	[nullMotion,Target,weight] = rbbc3(point,Priors,Mu,Sigma,m,weight,Start);


	%LLL = m.getConstCurvCoords(m.q);
	%TotalLength = sum(LLL) + m.Links*m.d0;  
	%[~,L1] = m.getCoords(modules);
	%[~,L2] = m.getCoords(fraction);
	%TotalLength = L1 - L2;
	%y = [TotalLength;Tip];
	%nullMotionLength = zeros(m.nbJVar,1);
	%for i=1:m.Links
	%	ddd(i) = LLL(i) - (m.L0)*(1+m.MaximumElong/2);
	%	VVV(:,i) = ddd(i) * m.Refs(:,3,i);
	%	nullMotionLength = nullMotionLength + pinv(m.JJ(i-1))*VVV(:,i);
	%end
	%nullMotion = 1/2*(nullMotion + nullMotionLength);
	%nullMotion = zeros(1,m.nbJVar)';
	m.basicMotion(Tip,direction,nullMotion,weight,10);
	%m.basicMotion(Tip,[0 0 1]',nullMotionLength,weight,5);
	%direction = [0 0 1]';
	
	[Tip,LTip] = m.getCoords(modules);
	Base = m.P(:,1);

	TotalLength = LTip + sqrt((Base-Start)'*(Base-Start));

	y = [TotalLength;Tip];

	delete(hhh);
	delete(hhh1);
	hhh = m.plot;
	%DP Kmeans with Data
	%Initialize at first point
	[Priors,Mu,Sigma,N] = OnlineEMDP2(N,y,minsigma,Priors,Mu,Sigma,lambda);
	model.Mu = Mu;
	model.Sigma = Sigma;
	model.Priors = Priors;
	%rGMR = GMR(TotalLength,1,[2:4],model);
	
	tt = linspace(0,TotalLength,100);
	rGMR = GMR(tt,1,[2:4],model);
	%LActivate = [L2,2*L2,3*L2];
	TrajAtt = rGMR.Mu;
	hhh1 = plot3(TrajAtt(1,:),TrajAtt(2,:),TrajAtt(3,:),'Linewidth',2,'color',[1 0.4 0.4]);
	hhh1 = [hhh1 plot3(Target(1,:),Target(2,:),Target(3,:),'x')];
	Ref = m.Refs(:,:,end);

	%hhh1 = plotGMM3D(Mu([1:3],:),Sigma([1:3],[1:3],:),[1 0 0],0.3);

	%end
	getData = getappdata(gcf,'getData');
	exitFlag = getappdata(gcf,'exitFlag');
	
end

hhh1 = plotGMM3D(Mu([2:4],:),Sigma([2:4],[2:4],:),[1 0 0],0.3);
delete(hhh)
pause

close all

end

function winDown(h,evt)
	setappdata(gcf,'getData',1);
	set(h,'WindowButtonMotionFcn',@winMove);
	set(h,'WindowButtonUpFcn',@winUp);
	set(h,'WindowScrollWheelFcn',@winWheel);
	if strcmp(get(gcf,'SelectionType'),'alt') == 1
		setappdata(gcf,'exitFlag',1);
		return
	end
	tic
end


function winUp(h,evt)
	setappdata(gcf,'getData',0);
end

function winWheel(h,evt)
	setappdata(gcf,'scrollCounts',evt.VerticalScrollCount);
end

function winMove(h,evt)
end

function readkeys(src,event)
	Tip = getappdata(gcf,'Tip');
	Ref = getappdata(gcf,'Ref');
	step = getappdata(gcf,'step');
	ViewPoint = getappdata(gcf,'ViewPoint');
	scrollCount = 0;
	if strcmp(event.Key,'uparrow') == 1
		Tip = Tip + step * Ref(:,3);
	elseif strcmp(event.Key,'downarrow') == 1
		Tip = Tip - step * Ref(:,3);
	elseif strcmp(event.Key,'leftarrow') == 1
		Tip = Tip - [step;0;0];
	elseif strcmp(event.Key,'rightarrow') == 1
		Tip = Tip + [step;0;0];
	elseif strcmp(event.Key,'pageup') == 1
		Tip = Tip + [0;step;0];
	elseif strcmp(event.Key,'pagedown') == 1
		Tip = Tip + [0;-step;0];
	elseif strcmp(event.Key,'z') == 1
		scrollCount = -1;
	elseif strcmp(event.Key,'x') == 1
		scrollCount = 1;
	elseif strcmp(event.Key,'q') == 1
		ViewPoint = ViewPoint + 1;
	elseif strcmp(event.Key,'a') == 1
		ViewPoint = ViewPoint - 1;
	elseif strcmp(event.Key,'w') == 1
		step = step + 0.001;
	elseif strcmp(event.Key,'s') == 1
		step = step - 0.001;
		if step == 0 
			step = 0.001;
		end
	end
	setappdata(gcf,'Tip',Tip);
	setappdata(gcf,'scrollCounts',scrollCount);
	setappdata(gcf,'step',step);
	setappdata(gcf,'ViewPoint',ViewPoint);
end

function lambda = EvalLambda(P)

	lambda = 1/10*(P'*P + 0.1);

end
