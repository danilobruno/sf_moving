function rGMR = GMR(x,in,out,model)

nbData = size(x,2);

Mu = model.Mu;
Sigma = model.Sigma;
Priors = model.Priors;

nbVar = size(Mu,1);
nbStates = size(Sigma,3);

rGMR.Mu = zeros(length(out),nbData);
rGMR.Sigma = zeros(length(out),length(out),nbData);
rGMR.Wp = zeros(length(out),length(out),nbData);
rGMR.H = zeros(nbStates,nbData);

for n=1:nbData
	for i=1:nbStates
		rGMR.H(i,n) = Priors(i) * gaussPDF(x(:,n),Mu(in,i),Sigma(in,in,i));
	end
	rGMR.H(:,n) = rGMR.H(:,n) / sum(rGMR.H(:,n));
	for i=1:nbStates
		MuTmp(:,i) = Mu(out,i) + Sigma(out,in,i)*inv(Sigma(in,in,i))*(x(:,n) - Mu(in,i));
		rGMR.Mu(:,n) = rGMR.Mu(:,n) + MuTmp(:,i) * rGMR.H(i,n);
		rGMR.Sigma(:,:,n) = rGMR.Sigma(:,:,n) + rGMR.H(i,n)^2 .* (Sigma(out,out,i) - (Sigma(out,in,i)*inv(Sigma(in,in,i))*Sigma(in,out,i)));
	end
	rGMR.Wp(:,:,n) = inv(rGMR.Sigma(:,:,n));
end


end
