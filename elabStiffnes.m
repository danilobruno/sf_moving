function [ output_args ] = elabStiffnes( input_args )
%ELABSTIFFNES Summary of this function goes here
%   Detailed explanation goes here

load('data/stiffness.mat');

kPSF = zeros(2,length(record));

set(0,'DefaultLineLineWidth',3)
set(0,'DefaultAxesFontSize', 14)

for i=1:length(record)
	St = record(i).Stiffness;
	for n = 1:size(St,3)
		kP(n,i) = 1/3*(St(1,1,n) + St(2,2,n) + St(3,3,n));
		if n<size(St,3)/2
			kPSF(1,i) = kPSF(1,i) + kP(n,i);
		else
			kPSF(2,i) = kPSF(2,i) + kP(n,i);
		end
	end
end

kPSF = kPSF/size(St,3)*2;

surf(kP);
xlabel('t');
ylabel('s');
zlabel('Stiffness');

tt = linspace(0,1,length(record));

figure
subplot(1,2,1)
plot(tt,kPSF(1,:));
title('Proximal');
xlabel('Time');
ylabel('Stiffness');
subplot(1,2,2)
plot(tt,kPSF(2,:));
title('Distal');
xlabel('Time')
ylabel('Stiffness');

figure
plot([1:size(St,3)],kP(:,30));

end

