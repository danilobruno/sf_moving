function [ output_args ] = ElabDataCollision( input_args )
%ELABDATA Summary of this function goes here
%   Detailed explanation goes here

load('data/dataCollision.mat');
totpoints = length(dataCollision);

n = 4;

m=StiffFlop(2);
m.kMidConst = 0;
%%%%%%%%%%%%%%%%%%
%%Environment
%%%%%%%%%%%%%%%%%%
offset = repmat([0.05,0,0.03]',1,3);
ObstMu = [-0.05 0.06 0; 0 0 0; 0.1 0.12 0.2] + offset;
ObstSigma(:,:,1) = [0.0003 0 0 ; 0 0.002 0 ; 0 0 0.001]; 
ObstSigma(:,:,2) = [0.0003 0 0 ; 0 0.002 0 ; 0 0 0.0010]; 
ObstSigma(:,:,3) = [0.0013 0 -0.0005 ; 0 0.002 0 ; -0.0005 0 0.0004]; 

%%2D Environment
offset2 = repmat([0.05,0.03]',1,3);
ObstMu2 = [-0.05 0.06 0; 0.1 0.12 0.2] + offset2;
ObstSigma2(:,:,1) = [0.0003  0 ;  0  0.001]; 
ObstSigma2(:,:,2) = [0.0003  0 ; 0  0.0010]; 
ObstSigma2(:,:,3) = [0.0013 -0.0005 ; -0.0005 0.0004]; 



%%%%%%%%%%%%%%%%%
%%Evaluation of random motion
%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%
%gmm
%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
q = dataCollision(n).gmmMotion';
P = zeros(3,size(q,1),5);
for i=1:size(q,1)
	m.setQ(q(i,:));
	P(:,i,1) = m.P(:,1);
	P(:,i,2) = m.getCoords(0.5,m.q);
	P(:,i,3) = m.P(:,2);
	P(:,i,4) = m.getCoords(1.5,m.q);
	P(:,i,5) = m.getCoords(2,m.q);
	L(:,i) = m.getConstCurvCoords(m.q);
end
%Evaluate collision

EvaluationGmm = EvaluateCollision1(P,q,ObstMu,ObstSigma);

q0 = dataCollision(n).pointsMotion';
P0 = zeros(3,size(q0,1),5);
for i=1:size(q0,1)
	m.setQ(q0(i,:));
	P0(:,i,1) = m.P(:,1);
	P0(:,i,2) = m.getCoords(0.5,m.q);
	P0(:,i,3) = m.P(:,2);
	P0(:,i,4) = m.getCoords(1.5,m.q);
	P0(:,i,5) = m.P(:,3);
	L0(:,i) = m.getConstCurvCoords(m.q);
end

EvaluationPoints = EvaluateCollision1(P0,q0,ObstMu,ObstSigma);

ttt = linspace(0,1,size(L,2));
ttt0 = linspace(0,1,size(L0,2));
%figure
%subplot(1,2,1)
%hold on
%
%plot(ttt,L(1,:),'color','black','linewidth',3);
%plot(ttt0,L0(1,:),'color','red','linewidth',3);
%
%subplot(1,2,2)
%hold on
%plot(ttt,L(2,:),'color','black','linewidth',3);
%plot(ttt0,L0(2,:),'color','red','linewidth',3);

figure
hold on
ratio = L(1,:)./L(2,:);
ratio0 = L0(1,:)./L0(2,:);
plot(ttt,ratio,'color','black','linewidth',3);
plot(ttt0,ratio0,'color','red','linewidth',3);
legend('rbbc','closest point');
title('Length ratio between the 2 sections','fontsize',20);
xlabel('Percentage of task completion','fontsize',14);
ylabel('Ratio','fontsize',14);

figure
subplot(1,4,1)
hold on
plot(P(1,:,1),P(3,:,1),'color','black','linewidth',3);
plot(P0(1,:,1),P0(3,:,1),'color','red','linewidth',3);
plotGMM(ObstMu2,ObstSigma2,[0.2 0 0],1);
legend('rbbc','closest point');
title('Point 0','fontsize',20);
subplot(1,4,2)
hold on
plot(P(1,:,2),P(3,:,2),'color','black','linewidth',3);
plot(P0(1,:,2),P0(3,:,2),'color','red','linewidth',3);
plotGMM(ObstMu2,ObstSigma2,[0.2 0 0],1);
legend('rbbc','closest point');
title('Point 0.5','fontsize',20);
subplot(1,4,3)
hold on
plot(P(1,:,3),P(3,:,3),'color','black','linewidth',3);
plot(P0(1,:,3),P0(3,:,3),'color','red','linewidth',3);
plotGMM(ObstMu2,ObstSigma2,[0.2 0 0],1);
legend('rbbc','closest point');
title('Point 1','fontsize',20);
subplot(1,4,4)
hold on
plot(P(1,:,4),P(3,:,4),'color','black','linewidth',3);
plot(P0(1,:,4),P0(3,:,4),'color','red','linewidth',3);
plotGMM(ObstMu2,ObstSigma2,[0.2 0 0],1);
legend('rbbc','closest point');
title('Point 1.5','fontsize',20);

%Evaluate collision

figure
subplot(1,2,1)
plot(ttt,EvaluationGmm(1,:),'color','red','Linewidth',3);
hold on
plot(ttt,EvaluationGmm(2,:),'Linewidth',3);
plot(ttt,EvaluationGmm(3,:),'Linewidth',3,'color','green');
plot(ttt,EvaluationGmm(4,:),'Linewidth',3,'color','magenta');
plot(ttt,EvaluationGmm(5,:),'Linewidth',3,'color','black');
legend('Base','0.5','Mid point','1.5','Tip');
xlabel('Percentage of task completion','fontsize',14);
ylabel('Distance index');
title('Distance from obstacles (rbbc)','fontsize',20);
plot(ttt,zeros(1,size(ttt,2)),'linewidth',4,'color','black');

%figure
%hold on
%plotGMM3D(ObstMu,ObstSigma,[0.2 0 0],1);
%view([0 -1 0]);
%axis([-0.2 0.2 -0.1 0.1 -0.2 0.3]);
%m = StiffFlop(2);
%axis equal
%TotalMotionEvaluation = dataCollision(n).gmmMotion;
%for i=1:size(TotalMotionEvaluation,2)
%	m.setQ(TotalMotionEvaluation(:,i)');
%	m.plot;
%	[xxCyl,yyCyl,zzCyl] = cylinder(m.Radius/2);
%	zzCyl(2,:) = -5*m.L0*zzCyl(2,:);
%	for i=1:size(xxCyl,1)
%		for j=1:size(xxCyl,2)
%			VTmp = [xxCyl(i,j);yyCyl(i,j);zzCyl(i,j)];
%			VTmp = m.Refs(:,:,1)*VTmp;
%			VTmp = VTmp + m.P(:,1);
%			xxCyl(i,j) = VTmp(1);
%			yyCyl(i,j) = VTmp(2);
%			zzCyl(i,j) = VTmp(3);
%		end
%	end
%	hhh2 = surf(xxCyl,yyCyl,zzCyl,'FaceColor',[0.7 0.7 0.7]);
%	hold on
%end
%
%
subplot(1,2,2)
plot(ttt0,EvaluationPoints(1,:),'color','red','Linewidth',3);
hold on
plot(ttt0,EvaluationPoints(2,:),'Linewidth',3);
plot(ttt0,EvaluationPoints(3,:),'Linewidth',3,'color','green');
plot(ttt0,EvaluationPoints(4,:),'Linewidth',3,'color','magenta');
plot(ttt0,EvaluationPoints(5,:),'Linewidth',3,'color','black');
legend('Base','0.5','Mid point','1.5','Tip');
ylabel('Distance index');
xlabel('Percentage of task completion','fontsize',14);
title('Distance from obstacles (closest point)','fontsize',20);
plot(ttt0,zeros(1,size(ttt0,2)),'linewidth',4,'color','black');

%figure
%hold on
%plotGMM3D(ObstMu,ObstSigma,[0.2 0 0],1);
%view([0 -1 0]);
%axis([-0.2 0.2 -0.1 0.1 -0.2 0.3]);
%m = StiffFlop(2);
%TotalMotionEvaluation = dataCollision(n).pointsMotion;
%axis equal
%for i=1:size(TotalMotionEvaluation,2)
%	m.setQ(TotalMotionEvaluation(:,i)');
%	m.plot;
%	[xxCyl,yyCyl,zzCyl] = cylinder(m.Radius/2);
%	zzCyl(2,:) = -5*m.L0*zzCyl(2,:);
%	for i=1:size(xxCyl,1)
%		for j=1:size(xxCyl,2)
%			VTmp = [xxCyl(i,j);yyCyl(i,j);zzCyl(i,j)];
%			VTmp = m.Refs(:,:,1)*VTmp;
%			VTmp = VTmp + m.P(:,1);
%			xxCyl(i,j) = VTmp(1);
%			yyCyl(i,j) = VTmp(2);
%			zzCyl(i,j) = VTmp(3);
%		end
%	end
%	hhh2 = surf(xxCyl,yyCyl,zzCyl,'FaceColor',[0.7 0.7 0.7]);
%	hold on
%end

%Evaluate trajectories
%\load('data/MotionGmm.mat');
%\
%\m=StiffFlop(2);
%\m.kMidConst = 0;
%\
%\for i=1:size(Motion,2)
%\	q = Motion(:,i)';
%\	m.setQ(q);
%\	P(:,i,1) = m.P(:,1);
%\	P(:,i,2) = m.P(:,2);
%\	L(:,i) = m.getConstCurvCoords(q);
%\end
%\clear('Motion');
%\load('data/MotionPoints.mat');
%\
%\for i=1:size(Motion,2)
%\	q = Motion(:,i)';
%\	m.setQ(q);
%\	P0(:,i,1) = m.P(:,1);
%\	P0(:,i,2) = m.P(:,2);
%\	L0(:,i) = m.getConstCurvCoords(q);
%\end
%\
%\figure
%\subplot(1,2,1)
%\hold on
%\plot([1:size(L,2)],L(1,:),'color','black','linewidth',3);
%\plot([1:size(L0,2)],L0(1,:),'color','red','linewidth',3);
%\
%\subplot(1,2,2)
%\hold on
%\plot([1:size(L,2)],L(2,:),'color','black','linewidth',3);
%\plot([1:size(L0,2)],L0(2,:),'color','red','linewidth',3);
%\
%\figure
%\hold on
%\ratio = L(1,:)./L(2,:);
%\ratio0 = L0(1,:)./L0(2,:);
%\plot([1:size(L,2)],ratio,'color','black','linewidth',3);
%\plot([1:size(L0,2)],ratio0,'color','red','linewidth',3);
%\
%\offset = repmat([0.05,0.03]',1,3);
%\ObstMu = [-0.05 0.06 0; 0.1 0.12 0.2] + offset;
%\ObstSigma(:,:,1) = [0.0003  0 ;  0  0.001]; 
%\ObstSigma(:,:,2) = [0.0003  0 ; 0  0.0010]; 
%\ObstSigma(:,:,3) = [0.0013 -0.0005 ; -0.0005 0.0004]; 
%\%
%\
%\figure
%\subplot(1,2,1)
%\hold on
%\plot(P(1,:,1),P(3,:,1),'color','black','linewidth',3);
%\plot(P0(1,:,1),P0(3,:,1),'color','red','linewidth',3);
%\plotGMM(ObstMu,ObstSigma,[0.2 0 0],1);
%\subplot(1,2,2)
%\hold on
%\plot(P(1,:,2),P(3,:,2),'color','black','linewidth',3);
%\plot(P0(1,:,2),P0(3,:,2),'color','red','linewidth',3);
%\plotGMM(ObstMu,ObstSigma,[0.2 0 0],1);
%\
%\
%\%Evaluate lenght
%\%
%\%Evaluate collision

nnn = linspace(0,1,200);
for n=1:totpoints
	q = dataCollision(n).gmmMotion';
	q0 = dataCollision(n).pointsMotion';
	PTmp = zeros(3,size(q,1),5);
	PTmp0 = zeros(3,size(q0,1),5);
	L = zeros(2,size(q,1));
	L0 = zeros(2,size(q0,1));
	for i=1:size(q,1)
		m.setQ(q(i,:));
		L(:,i) = m.getConstCurvCoords(m.q);
		PTmp(:,i,1) = m.P(:,1);
		PTmp(:,i,2) = m.getCoords(0.5);
		PTmp(:,i,3) = m.P(:,2);
		PTmp(:,i,4) = m.getCoords(1.5);
		PTmp(:,i,5) = m.P(:,3);
	end
	for i=1:size(q0,1)
		m.setQ(q0(i,:));
		PTmp0(:,i,1) = m.P(:,1);
		PTmp0(:,i,2) = m.getCoords(0.5,m.q);
		PTmp0(:,i,3) = m.P(:,2);
		PTmp0(:,i,4) = m.getCoords(1.5);
		PTmp0(:,i,5) = m.P(:,3);
		L0(:,i) = m.getConstCurvCoords(m.q);
	end
	ttt = linspace(0,1,size(L,2));
	ttt0 = linspace(0,1,size(L0,2));
	ratio = L(1,:)./L(2,:);
	ratio0 = L0(1,:)./L0(2,:);
	ratioNorm(n,:) = spline(ttt,ratio,nnn);
	ratioNorm0(n,:) = spline(ttt0,ratio0,nnn);
	Collision = EvaluateCollision1(PTmp,q,ObstMu,ObstSigma);
	Collision0 = EvaluateCollision1(PTmp0,q0,ObstMu,ObstSigma);
	for jj=1:5
		Dist(n,:,jj) = spline(ttt,Collision(jj,:),nnn);
		Dist0(n,:,jj) = spline(ttt0,Collision0(jj,:),nnn);
	end
end

ratioMean = mean(ratioNorm);
ratioMean0 = mean(ratioNorm0);
ratioSigma = var(ratioNorm);
ratioSigma0 = var(ratioNorm0);

for jj=1:5
	DistMean(jj,:) = mean(Dist(:,:,jj));
	DistMean0(jj,:) = mean(Dist0(:,:,jj));
	DistSigma(jj,:) = var(Dist(:,:,jj));
	DistSigma0(jj,:) = var(Dist0(:,:,jj));
	ddMax(jj,:) = DistMean(jj,:) + 3*DistSigma(jj,:);
	ddMin(jj,:) = DistMean(jj,:) - 3*DistSigma(jj,:);
	ddMax0(jj,:) = DistMean0(jj,:) + 3*DistSigma0(jj,:);
	ddMin0(jj,:) = DistMean0(jj,:) - 3*DistSigma0(jj,:);
end

rrMax = ratioMean + 3*ratioSigma;
rrMin = ratioMean - 3*ratioSigma;
rrMax0 = ratioMean0 + 3*ratioSigma0;
rrMin0 = ratioMean0 - 3*ratioSigma0;


figure
plot(nnn,ratioMean,'color','black','linewidth',3);
hold on
plot(nnn,ratioMean0,'color','red','linewidth',3);
%patch([nnn fliplr(nnn)],[rrMin fliplr(rrMax)],[0.7 0.7 0.7 ],'EdgeColor','none');hold on
%patch([nnn fliplr(nnn)],[rrMin0 fliplr(rrMax0)],[1 0.7 0.7 ],'EdgeColor','none');hold on

figure
for jj=1:4
	subplot(1,4,jj)
	plot(nnn,DistMean(jj,:),'color','black','linewidth',3);
	hold on
	plot(nnn,DistMean0(jj,:),'color','red','linewidth',3);
	patch([nnn fliplr(nnn)],[ddMin(jj,:) fliplr(ddMax(jj,:))],[0.7 0.7 0.7 ],'EdgeColor','none');hold on
	patch([nnn fliplr(nnn)],[ddMin0(jj,:) fliplr(ddMax0(jj,:))],[1 0.7 0.7 ],'EdgeColor','none');hold on
end
	
figure
	ccc = [1 0 0 ; 0 1 0 ; 0 0 1; 1 0 1];
	subplot(1,2,1)
	for jj=1:4
		plot(nnn,DistMean(jj,:),'color',ccc(jj,:),'linewidth',3);
		patch([nnn fliplr(nnn)],[ddMin(jj,:) fliplr(ddMax(jj,:))],[0.7 0.7 0.7 ],'EdgeColor','none');hold on
		hold on
	end
	subplot(1,2,2)
	for jj=1:4
		plot(nnn,DistMean0(jj,:),'color',ccc(jj,:),'linewidth',3);hold on
		patch([nnn fliplr(nnn)],[ddMin0(jj,:) fliplr(ddMax0(jj,:))],[1 0.7 0.7 ],'EdgeColor','none');hold on
	end


end

function [Evaluation] = EvaluateCollision(P,q,ObstMu,ObstSigma) 
Evaluation = zeros(size(P,3),size(q,1));
for i = 1:size(q,1)
	for j = 1:size(P,3)
		CollisionV = zeros(1,size(ObstSigma,3));
		for ii=1:size(ObstSigma,3)
			CollisionV(1,ii) = mvnpdf(P(:,i,j),ObstMu(:,ii),ObstSigma(:,:,ii));
		end
		Evaluation(j,i) = max(CollisionV);
		Evaluation(j,i) = exp(-Evaluation(j,i)/350) - exp(-1);

	end
end

end

function [Evaluation] = EvaluateCollision1(P,q,ObstMu,ObstSigma)
Evaluation = zeros(size(P,3),size(q,1));
ObstSigma2 = ObstSigma([1,3],[1,3],:);
for i = 1:size(q,1)
	for j = 1:size(P,3)
		CollisionV = zeros(1,size(ObstSigma,3));
		for ii=1:size(ObstSigma,3)
			[V,D] = eig(ObstSigma2(:,:,ii));
			VV = zeros(3,2);
			VV(1,:) = V(1,:);
			VV(3,:) = V(2,:);
			CollisionV(1,ii) = distanceEllipsePoints(P(:,i,j),sqrt(3*D(1,1)),sqrt(3*D(2,2)),ObstMu(:,ii)',VV(:,1)',VV(:,2)');
		end
		Evaluation(j,i) = min(CollisionV);
	end
end

end

