function weights = WeightEval(s,L0)
%TEST Summary of this function goes here
%   Detailed explanation goes here

MuW = [L0,L0*0.4,L0*0.8];

SigmaW = [0.000001,0.0001,0.000001];

t = linspace(L0,L0*0.8,100);

for i=1:3
	h(:,i) = gaussPDF(s,MuW(i),SigmaW(i));
end
h = h./repmat(sum(h,2),1,3);

h = h/2+1/2;

weights = [1 1 h];

end

