function [dq,Target,weight] = rbbc2(point,Priors,Mu,Sigma,m,weight,StartPos,StartOr)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

%m if a SiffFlop object

Target = zeros(3,1);

model.Mu = Mu;
model.Sigma = Sigma;
model.Priors = Priors;
modules = m.Links;
discret = 10;
L2 = (m.L0*1.3+2*m.d0);
LActivate = modules*ones(1,size(point,2));
LActivate = LActivate - point;
LActivate = L2*LActivate;

%distBase = sqrt((m.P(:,1)-Start)'*(m.P(:,1)-Start));	%Distance of base from starting point
%LActivate = LActivate + repmat(distBase,1,length(point));

P1 = zeros(3,length(point));
for ii = 1:length(point)
	P1(:,ii) = m.getCoords(point(ii));
end

LLL = m.getConstCurvCoords(m.q);
SecLength = LLL/m.L0;
[Tip,TotalLength] = m.getCoords(modules);
Ref = m.getTipFrame;

in = 1;
out = [2:4];
dq = zeros(m.nbJVar,1);

zz = zeros(1,modules*discret);
for i=1:modules*discret
	P = m.getCoords(i/discret) - StartPos;
	zz(i) = P'*StartOr;
end
zzTmp = abs(zz);
[~,index] = min(zzTmp); %index of closest value
fraction = index/discret;

[P,LengthTrocar] = m.getCoords(fraction);
Linside = TotalLength - LengthTrocar;

if (m.P(3,1)-StartPos)'*StartOr > 0  
	Linside = Linside + sqrt((m.P(:,1)-StartPos)'*(m.P(:,1)-StartPos));
end

V = zeros(3,1);
J = m.jacobian;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Eventual controller for trocar port
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%	JJTrocar = m.JJ(fraction);
%	if fraction > 0.1 
%		V([1,2],1) = -P([1,2],1); %Distance of point from trocar
%	else
%		V(:,1) = -P(:,1); %Distance of point from trocar
%		weigth(2) = 0;
%	end
%	N = null(J);
%	W = JJTrocar*N;
%	dq = N*(pinv(W)*V);
	%dq = pinv(JJTrocar)*V;
%	dqTrocar = dq;
%	if fraction > point(1)
%		dq = zeros(m.nbJVar,1);
%	end
	%dq = zeros(m.nbJVar,1);
	%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%Evaluate displacement for every chosen point
for ii = 1:length(point)
	P10 = P1(:,ii);  						%Previous point 
	P1(:,ii) = m.getCoords(point(ii)); 		%Current point
	V1(:,ii) = P1(:,ii) - P10; 				%Velocity of current point
	ll(ii) = Linside - LActivate(ii); 		%Distance from tip of current point
	out = GMR(ll(ii),1,[2:4],model); 		%Desired position of current point
	Tar = zeros(3,1); 						%Project on plane (only for 2D simulation)
	Tar(1,1) = out.Mu(1,1);
	Tar(3,1) = out.Mu(3,1);
	Target(:,ii) = Tar;
	WP = zeros(3); 							%Covariance (for force control)
	WP(1,1) = out.Wp(1,1);
	WP(1,3) = out.Wp(1,3);
	WP(3,1) = out.Wp(3,1);
	WP(3,3) = out.Wp(3,3);
	
	%%Evaluate weight of point displacement based on variability
	prob = gaussPDF(P1([1:3],ii),out.Mu,out.Sigma)/gaussPDF(out.Mu,out.Mu,out.Sigma);
	
	Coeff = (1 - prob);
	%Coeff = 1;
	
	%Proportional positional controller for given point 
	VV = Coeff*(Tar - P1(:,ii));% - 2*sqrt(Coeff)*V1(:,ii);
	%Critically damped positional controller for given 
	%VV = Coeff*(Tar - P1(:,ii)) - 2*sqrt(Coeff)*V1(:,ii);
	
	%VV(3,1) = 0;
	%VV = VV - (VV'*Vel(:,index+1))*Vel(:,index+1)/(Vel(:,index+1)'*Vel(:,index+1));
	%m.movePoint(point(i),V,10);
	
	%Evaluate joint command corresponding to point displacement
	JJ = m.JJ(point(ii));
	%Evaluate projection in task null space
	N = null(J);
	W = JJ*N;
	%Add to other points displacement
	dq = dq + N*(pinv(W)*VV);
	%N = null([J;JJTrocar]);
	%W = JJ*N;
	%dq = dq + N*(pinv(W)*VV);
	%
	%dq = dq + (eye(m.nbJVar) - pinv(JJTrocar)*JJTrocar)*pinv(JJ)*VV;
	%dq = dq + pinv(JJ)*VV;
	%dq = dq + (eye(m.nbJVar) - pinv(JJ)*JJ)*dqTrocar;
	%dq = zeros(m.nbJVar,1);
	%plot3(Tar(1),Tar(2),Tar(3),'o');
	%quiver3(P1(1),P1(2),P1(3),VV(1),VV(2),VV(3));
	%plot3(P1(1),P1(2),P1(3),'o');
	%
end

%Average displacements
dq = dq/(length(point));

end


%function [V,dq] = nullLength(m)
%%NULLVIEW Summary of this function goes here
%%   Detailed explanation goes here
%
%N = null(m.jacobian);
%Refs = m.Refs;
%dq = zeros(m.nbJVar,1);
%for i=1:m.Links-1
%	JJ(:,:,i) = m.JJ(i);
%	LL = (pinv(JJ(:,:,i))*Refs(:,3,i+1))'*N;
%	dqn(:,i) = (LL*N')';
%	V(:,i) = JJ(:,:,i)*dqn(:,i);
%	V(:,i) = V(:,i) / sqrt(V(:,i)'*V(:,i));
%	dq = dq + dqn(:,i);
%end
%
%end

