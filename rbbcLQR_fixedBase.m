function [dq,Target,weight,Stiffness] = rbbcLQR_fixedBase(point,Priors,Mu,Sigma,m,weight)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here

Target = zeros(3,1);

model.Mu = Mu;
model.Sigma = Sigma;
model.Priors = Priors;
modules = m.Links;
discret = 10;
%Evaluate L2 at each step as 1/modules*TotalLength
%L2 = (m.L0*1.3+2*m.d0);
L2 = 1/modules*TotalLength;
%Evaluate desired activation for each controlled point
LActivate = modules*ones(1,size(point,2));
LActivate = LActivate - point;
LActivate = L2*LActivate;

%Current robot status
%Selected points position
P1 = zeros(3,length(point));
for ii = 1:length(point)
	P1(:,ii) = m.getCoords(point(ii));
end

LLL = m.getConstCurvCoords(m.q);
SecLength = LLL/m.L0;
[Tip,TotalLength] = m.getCoords(modules);
Ref = m.getTipFrame;

%distBase = sqrt((m.P(:,1)-Start)'*(m.P(:,1)-Start));	%Distance of base from starting point
%LActivate = LActivate + repmat(distBase,1,length(point));

in = 1;
out = [2:4];
dq = zeros(m.nbJVar-6,1); %Remove joint coordinates of basis

%Trocar is base frame 

%zz = zeros(1,modules*discret);
%for i=1:modules*discret
%	P = m.getCoords(i/discret) - StartPos;
%	zz(i) = P'*StartOr;
%end
%zzTmp = abs(zz);
%[~,index] = min(zzTmp); %index of closest value
%fraction = index/discret;
%
%[P,LengthTrocar] = m.getCoords(fraction);
%Linside = TotalLength - LengthTrocar;

%if (m.P(3,1)-StartPos)'*StartOr > 0  
%	Linside = Linside + sqrt((m.P(:,1)-StartPos)'*(m.P(:,1)-StartPos));
%end

V = zeros(3,1);
J = m.jacobian;

JFixed = J(:,[7:end]); %Jacobian without basis motion

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Eventual controller for trocar port
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%	JJTrocar = m.JJ(fraction);
%	if fraction > 0.1 
%		V([1,2],1) = -P([1,2],1); %Distance of point from trocar
%	else
%		V(:,1) = -P(:,1); %Distance of point from trocar
%		weigth(2) = 0;
%	end
%	N = null(J);
%	W = JJTrocar*N;
%	dq = N*(pinv(W)*V);
	%dq = pinv(JJTrocar)*V;
%	dqTrocar = dq;
%	if fraction > point(1)
%		dq = zeros(m.nbJVar,1);
%	end
	%dq = zeros(m.nbJVar,1);
	%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%Evaluate trajectory of attractors using LQR over s
rFactor = 1E-1;
A = kron([0 1; 0 0], eye(3));
B = kron([0; 1], eye(3));
%Initialize Q and R weighting matrices
Q = zeros(6,6);
R = eye(3) * rFactor;
%TODO
TotPoints = 100;

%Remove initialization outside

%sMin = -3*(m.Links)*L2;
sMin = 0;
sMax = TotalLength
s = linspace(sMin,sMax,TotPoints); 		%From the starting position of the base to the tip 
ds = abs(sMax - sMin)/TotPoints;
%ds = L2/TotPoints;
out = GMR(s,1,[2:4],model); 		%Desired position of current point
dx = zeros(3,1);
x = m.P(:,end);
currDist = 0; 		%Distance from tip
ll = TotalLength;
pointSel = ones(1,length(point));
Target = zeros(3,length(point));
%Use only partial J (without base) TODO
N = null(JFixed);
Stiffness = zeros(3,6,TotPoints);
S = zeros(6,6);
for t=TotPoints:-1:1
	Q(1:3,1:3) = inv(out.Sigma(:,:,t)); 
	%Infinite horizon
	S = care(A, B, (Q+Q')/2, R); %(Q+Q')/2 is used instead of Q to avoid warnings when testing the symmetry of Q 
	%Finite horizon
	%S = S + ds * (A'*S + S*A - S * B * (R\B') * S + Q); 
	L = R\B' * S;
	Stiffness(:,:,t) = L;
	%Compute acceleration
	ddx =  -L * [x-out.Mu(:,t); dx];
	%Update velocity and position
	dx = dx + ddx * ds;
	ll = ll - sqrt(dx'*dx)*ds;
	currDist = currDist + sqrt(dx'*dx)*ds;
	x = x + dx * ds;
	for ii=1:length(point)
		if (pointSel(ii) == 1) & (currDist > LActivate(ii)) 
			pointSel(ii) = 0;
			Target(:,ii) = x;
			P1 = m.getCoords(point(ii)); 		%Current point
			VV = (Target(:,ii) - P1);% - 2*sqrt(Coeff)*V1(:,ii);
			%Evaluate joint command corresponding to point displacement
			JJ = m.JJ(point(ii));
			%Remove base DOF
			JJFixed = JJ(:,[7:end]);
			%Evaluate projection in task null space
			%W = JJ*N;
			W = JJFixed*N;
			%Add to other points displacement
			dq = dq + N*(pinv(W)*VV);
		end
	end
end
dq = dq/(length(point));

dq = [zeros(6,1);dq];

%%Visualize h_i(t) to see multiple choices TODO
%Target = [Tar];
Target(2,:) = zeros(1,length(point));
%Tar represents the target shape for the robot

end



