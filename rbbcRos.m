classdef rbbcRos < handle
    %RBBCROS Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        node;       %Node Handle
        m;          %Stiff-Flop object
        sub_j1;     %Subscriber to joint states 
        sub_j2;     %Subscriber to joint states 
		sub_tip;	%Subscriber to tip command
        pub_j1;      %Publisher on joint states
		pub_j2;
		pub_base;	%Publisher of base Pose	
        pub_tip;
        gmm;        %GMM modeling the variability
        point;      %Control points chosen to control the body
        lambda;     %Parameter for online GMM encoding
        minSigma;   %Minimum covariance
		q_status;	%Current status of joints in simulator
		q_command;	%Joint command
		
		StartPos;
		StartOr;
		weights;

		%Messages
		PMsg;		%Position message
		OMsg;		%Orientation message
		PoseMsg;	%Pose message
		BaseMsg;    %Base Pose
        

		Magnification;


    end
    
    methods
        %Constructor
        function obj = rbbcRos(node,point)
        
            obj.node = node;
            modules = 2;
            %Instantiate SF object
            obj.m = StiffFlop(modules);
			
            
            %Setting parameters manually (to update)
            obj.m.L0 = 0.05;
            obj.m.d0 = 0.015;
            obj.m.Radius = 0.02;
			q = obj.m.q;
            q([1:6]) = zeros(1,6);
			q(9) = 0.05;
			q(12) = 0.05;
            obj.m.setQ(q);
            obj.q_status = obj.m.q;
            %Initialize DP-Kmeans
            obj.lambda = obj.m.L0*1.8;
            %lambda = m.L0;
            obj.minSigma = obj.lambda/300;
            
            %Get status from robot (init) and pass to robot
            %obj.sub_j1 = rosmatlab.subscriber('Joint1_Matlab','geometry_msgs/Pose',1,obj.node);
            %obj.sub_j2 = rosmatlab.subscriber('Joint2_Matlab','geometry_msgs/Pose',1,obj.node);
            %obj.sub_j1.setOnNewMessageListeners({@obj.sub_j1_callback});
            %obj.sub_j2.setOnNewMessageListeners({@obj.sub_j2_callback});
            obj.pub_j1 = rosmatlab.publisher('sf_position_orientation_controller_1/command','geometry_msgs/Pose',obj.node);
            obj.pub_j2 = rosmatlab.publisher('sf_position_orientation_controller_2/command','geometry_msgs/Pose',obj.node);
			obj.pub_base = rosmatlab.publisher('base_control/command','geometry_msgs/Pose',obj.node);
            
            
			obj.sub_tip = rosmatlab.subscriber('multi_segment_control/command','geometry_msgs/Pose',10,obj.node);
			obj.sub_tip.setOnNewMessageListeners({@obj.sub_tip_callback});
            
            obj.point = point;

            obj.PoseMsg = rosmatlab.message('geometry_msgs/Pose',obj.node);
            obj.BaseMsg = rosmatlab.message('geometry_msgs/Pose',obj.node);
			obj.PMsg = rosmatlab.message('geometry_msgs/Point',obj.node);
			obj.OMsg = rosmatlab.message('geometry_msgs/Quaternion',obj.node);
        	

			obj.Magnification = 1;
        end
        
        function obj = init(obj)
            
			%obj.m.q = obj.q_status;

            %Initialize GMM with SF status
            N = 100;
            modules = 2;
            ss = linspace(0,modules,N);
            [~,TotalLength] = obj.m.getCoords(modules);
            for i=1:N
                [PTmp(:,i),LengthTmp(:,i)] = obj.m.getCoords(ss(i));
            end
            DataInit(1,:) = - repmat(TotalLength,1,size(TotalLength,2)) + LengthTmp;
            DataInit([2:4],:) = PTmp;
           
            [Priors,Mu,Sigma] = EM_init_regularTiming(DataInit,modules,obj.m.d0/100);
            [Priors,Mu,Sigma] = EM(DataInit,Priors,Mu,Sigma,obj.m.d0/100);
            %Initialize publisher of joint command
            obj.gmm.Priors = Priors;
            obj.gmm.Mu = Mu;
            obj.gmm.Sigma = Sigma;

			obj.StartPos = obj.m.P(:,end);
			obj.StartOr = obj.m.Refs(:,3,end);
			obj.weights = [1 1 1 1];
            
        end
        
        %Callback for suq_q_init
        function obj = sub_j1_callback(obj,msg)
            %Get joint states
			
            %q = obj.q_status;
			q = obj.m.q;

			obj.PMsg = msg.getPosition;
            q(7) = obj.PMsg.getX();
            q(8) = obj.PMsg.getY();
            q(9) = obj.PMsg.getZ();
            
            %obj.q_status = q;
			%
			obj.m.q = q;
            %obj.sub_j1.setOnNewMessageListeners({});
			obj.sub_j1.delete();
            %Modify status of robot
            
            
        end
        
        function obj = sub_j2_callback(obj,msg)
            %Get joint states
           
			%q = obj.q_status;
			q = obj.m.q;

			obj.PMsg = msg.getPosition;
            q(10) = obj.PMsg.getX();
            q(11) = obj.PMsg.getY();
            q(12) = obj.PMsg.getZ();

            %obj.q_status = q;
			%
			obj.m.q = q;
            %obj.sub_j2.setOnNewMessageListeners({});
			obj.sub_j2.delete();
            %Modify status of robot
            
        end
        
		function obj = sub_tip_callback(obj,msg)
			
			modules = 2;

			%New tip position
			obj.PMsg = msg.getPosition();
			P(1,1) = obj.PMsg.getX(); 
			P(2,1) = obj.PMsg.getY(); 
			P(3,1) = obj.PMsg.getZ();
			%New tip relative orientation
			obj.OMsg = msg.getOrientation();
			ww = obj.OMsg.getW();
			xx = obj.OMsg.getX();
			yy = obj.OMsg.getY();
			zz = obj.OMsg.getZ();
			
			theta = 2*acos(ww);
			if theta == 0 
				Z = [0;0;1];
			else
				ux = xx/(1-ww^2);
				uy = yy/sqrt(1-ww^2);
				uz = zz/sqrt(1-ww^2);
				Z = [sin(theta)*uy;-sin(theta)*ux;cos(theta)];
			end


			%Evaluate null space motion
			[nullMotion,Target,obj.weights] = rbbc2(obj.point,obj.gmm.Priors,obj.gmm.Mu,obj.gmm.Sigma,obj.m,obj.weights,obj.StartPos,obj.StartOr);
            
           
            
			%obj.m.basicMotion(P,[0 0 1]',zeros(12,1),[0 0 1 1],5);
			%obj.m.basicMotion(P,Z,zeros(12,1),[1 1 1 1],5);
			obj.m.basicMotion(P,Z,nullMotion,obj.weights,5);

			q = obj.m.q;
			obj.PMsg.setX(q(4)*obj.Magnification);
			obj.PMsg.setY(q(5)*obj.Magnification);
			obj.PMsg.setZ(q(6)*obj.Magnification);
			%Rot = StiffFlop.rotMatrix(q([1:3]));
			%quat = StiffFlop.RotToQuat(Rot);
			quat = [1 0 0 0];
            obj.OMsg.setW(quat(1));
			obj.OMsg.setX(quat(2));
			obj.OMsg.setY(quat(3));
			obj.OMsg.setZ(quat(4));
			obj.BaseMsg.setPosition(obj.PMsg);
			obj.BaseMsg.setOrientation(obj.OMsg);
			obj.pub_base.publish(obj.BaseMsg);

			obj.PMsg.setX(q(7));
			obj.PMsg.setY(q(8));
			obj.PMsg.setZ(q(9));
            obj.PoseMsg.setPosition(obj.PMsg);
            obj.pub_j1.publish(obj.PoseMsg);
			obj.PMsg.setX(q(10));
			obj.PMsg.setY(q(11));
			obj.PMsg.setZ(q(12));
            obj.PoseMsg.setPosition(obj.PMsg);
            obj.pub_j2.publish(obj.PoseMsg);

			discret= 10;
			zz = zeros(1,modules*discret);
			for i=1:modules*discret
				PPP = obj.m.getCoords(i/discret) - obj.StartPos;
				zz(i) = PPP'*obj.StartOr;
			end
			%[~,index] = min(zz<0);
			zzTmp = abs(zz);
			[~,index] = min(zzTmp); %index of closest value
			fraction = index/discret;
			[~,L1] = obj.m.getCoords(modules);
			[~,L2] = obj.m.getCoords(fraction);
			TotalLength = L1 - L2;
			if (obj.m.P(:,1)-obj.StartPos)'*obj.StartOr > 0
				TotalLength = TotalLength + sqrt((obj.m.P(:,1)-obj.StartPos)'*(obj.m.P(:,1)-obj.StartPos));
			end
			y = [TotalLength;P];
			[obj.gmm.Priors,obj.gmm.Mu,obj.gmm.Sigma] = OnlineEMDP2(10,y,obj.minSigma,obj.gmm.Priors,obj.gmm.Mu,obj.gmm.Sigma,obj.lambda);

		end

    end
    
end

