function [ output_args ] = PlotRndEvaluation( input_args )
%PLOTRNDEVALUATION Summary of this function goes here
%   Detailed explanation goes here
%

load('data/Collision.mat')
vv = exp(-Evaluation/250) - exp(-1)
figure
plot([1:size(vv,2)],vv(1,:),'color','red','Linewidth',3);
hold on
plot([1:size(vv,2)],vv(2,:),'Linewidth',3);
plot([1:size(vv,2)],vv(3,:),'Linewidth',3,'color','green');
legend('Tip','Mid point','Base');
xlabel('Time');
ylabel('Distance index');
title('Distance from obstacles');

figure
hold on
offset = repmat([0.05,0,0.03]',1,3);
ObstMu = [-0.05 0.06 0; 0 0 0; 0.1 0.12 0.2] + offset;
ObstSigma(:,:,1) = [0.0003 0 0 ; 0 0.002 0 ; 0 0 0.001]; 
ObstSigma(:,:,2) = [0.0003 0 0 ; 0 0.002 0 ; 0 0 0.0010]; 
ObstSigma(:,:,3) = [0.0013 0 -0.0005 ; 0 0.002 0 ; -0.0005 0 0.0004]; 
%
plotGMM3D(ObstMu,ObstSigma,[0.2 0 0],1);
view([0 -1 0]);
axis([-0.2 0.2 -0.1 0.1 -0.2 0.3]);
m = StiffFlop(2);
axis equal
for i=1:size(vv,2)
	m.setQ(TotalMotionEvaluation(:,i)');
	m.plot;
	[xxCyl,yyCyl,zzCyl] = cylinder(m.Radius/2);
	zzCyl(2,:) = -5*m.L0*zzCyl(2,:);
	for i=1:size(xxCyl,1)
		for j=1:size(xxCyl,2)
			VTmp = [xxCyl(i,j);yyCyl(i,j);zzCyl(i,j)];
			VTmp = m.Refs(:,:,1)*VTmp;
			VTmp = VTmp + m.P(:,1);
			xxCyl(i,j) = VTmp(1);
			yyCyl(i,j) = VTmp(2);
			zzCyl(i,j) = VTmp(3);
		end
	end
	hhh2 = surf(xxCyl,yyCyl,zzCyl,'FaceColor',[0.7 0.7 0.7]);
	hold on
end
%
%

load('data/CollisionCmp.mat')
vv = exp(-Evaluation/250) - exp(-1)
figure
plot([1:size(vv,2)],vv(1,:),'color','red','Linewidth',3);
hold on
plot([1:size(vv,2)],vv(2,:),'Linewidth',3);
plot([1:size(vv,2)],vv(3,:),'Linewidth',3,'color','green');
legend('Tip','Mid point','Base');
xlabel('Time');
ylabel('Distance index');
title('Distance from obstacles');

figure
hold on
offset = repmat([0.05,0,0.03]',1,3);
ObstMu = [-0.05 0.06 0; 0 0 0; 0.1 0.12 0.2] + offset;
ObstSigma(:,:,1) = [0.0003 0 0 ; 0 0.002 0 ; 0 0 0.001]; 
ObstSigma(:,:,2) = [0.0003 0 0 ; 0 0.002 0 ; 0 0 0.0010]; 
ObstSigma(:,:,3) = [0.0013 0 -0.0005 ; 0 0.002 0 ; -0.0005 0 0.0004]; 
%
plotGMM3D(ObstMu,ObstSigma,[0.2 0 0],1);
view([0 -1 0]);
axis([-0.2 0.2 -0.1 0.1 -0.2 0.3]);
m = StiffFlop(2);
axis equal
for i=1:size(vv,2)
	m.setQ(TotalMotionEvaluation(:,i)');
	m.plot;
	[xxCyl,yyCyl,zzCyl] = cylinder(m.Radius/2);
	zzCyl(2,:) = -5*m.L0*zzCyl(2,:);
	for i=1:size(xxCyl,1)
		for j=1:size(xxCyl,2)
			VTmp = [xxCyl(i,j);yyCyl(i,j);zzCyl(i,j)];
			VTmp = m.Refs(:,:,1)*VTmp;
			VTmp = VTmp + m.P(:,1);
			xxCyl(i,j) = VTmp(1);
			yyCyl(i,j) = VTmp(2);
			zzCyl(i,j) = VTmp(3);
		end
	end
	hhh2 = surf(xxCyl,yyCyl,zzCyl,'FaceColor',[0.7 0.7 0.7]);
	hold on
end
end

