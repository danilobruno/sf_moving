function [ output_args ] = mouseData_time( input_args )
%MOUSEDATA Summary of this function goes here
%   Detailed explanation goes here

Time = [];
Data = [];

nbVar = 3;
exitFlag = 0;

fig = figure('position',[10 170 550 550]); hold on; box on;

getData = 0;

set(0,'CurrentFigure',fig);


set(fig,'WindowButtonDownFcn',@winDown);
setappdata(gcf,'getData',getData);
setappdata(gcf,'exitFlag',exitFlag);

%Initialize DP-Kmeans
K=1;
lambda = 0.1;
sigma = lambda*eye(3);
minsigma = sigma/100;

while exitFlag == 0
	figure(fig)
	while getData == 1
		figure(fig); hold off;
		point = get(gca,'CurrentPoint');
		getData = getappdata(gcf,'getData');
		P = point(1,[1:2])';
		y = [toc;P];
		Data = [Data y];
		plot(Data(2,:),Data(3,:),'k.','markerSize',2);hold on;
		plot(0,0,'.');
		plot(2,2,'.');

		N = size(Data,2);
		%DP Kmeans with Data
		%Initialize at first point
		if N == 1
			cl(K).mu = y;
			cl(K).sigma = EvalLambda(y([2:3]))/100*eye(3);
			cl(K).n=1;
			c = 1;
		else
			d = zeros(1,K+1);
			for k=1:K
				d(k) = (y-cl(k).mu)'*(y-cl(k).mu);
			end
			%d(K+1) = lambda;
			d(K+1) = EvalLambda(P);
			[~,a] = min(d);
			c(N) = a;
			if a==K+1
				K = K+1;
				cl(K).mu = y;
				cl(K).sigma = EvalLambda(y([2:3]))/1000*eye(3);
				cl(K).n = 1;
			else
				cl(a).n = cl(a).n +1;
			end
		end

		for k = 1:K
			ycl = Data(:,c==k); %Data belonging to cluster
			cn = cl(k).n;    %Number of objects in cluster
			if cn > 1
				cl(k).mu = mean(ycl')' ;
				lambda = EvalLambda(cl(k).mu([2:3]));
				cl(k).sigma = 3*cov(ycl')+ lambda/100*eye(3);
			else
				cl(k).mu = ycl;
				cl(k).sigma = EvalLambda(ycl([2:3]))/1000*eye(3);
			end
		end

		Mu = zeros(nbVar,K);
		Sigma = zeros(nbVar,nbVar,K);
		Priors = zeros(1,K);
		for k=1:K
			Mu(:,k) = cl(k).mu;
			Sigma(:,:,k) = cl(k).sigma;
			Priors(k) = cl(k).n/N;
		end

		plotGMM(Mu([2:3],:),Sigma([2:3],[2:3],:),[1 0 0],1);

	end
	getData = getappdata(gcf,'getData');
	exitFlag = getappdata(gcf,'exitFlag');
end

Data
close all

%nbData = N;
%nbStates = K;
%
%%% P(X|t) GMR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%in=[1]; out=[2:3];
%expData(in,:) = linspace(min(Data(1,:)), max(Data(1,:)), nbData);
%expData(out,:) = zeros(length(out),nbData);
%expSigma = zeros(length(out),length(out),nbData);
%for n=1:nbData
%  %Weight
%  for i=1:nbStates
%    h(i) = gaussPDF(expData(in,n), Mu(in,i), Sigma(in,in,i));
%  end
%  h = h/sum(h);
%  for i=1:nbStates
%    %Compute expected conditional means
%    expData(out,n) = expData(out,n) + h(i) .* (Mu(out,i) + Sigma(out,in,i)*inv(Sigma(in,in,i)) * (expData(in,n)-Mu(in,i)));
%    %Compute expected conditional covariance matrices
%    expSigma(:,:,n) = expSigma(:,:,n) + h(i)^2 .* (Sigma(out,out,i) - (Sigma(out,in,i)*inv(Sigma(in,in,i))*Sigma(in,out,i)));
%  end
%end
%%GMR
%%
%figure()
%hold on;
%plotGMM(expData([2,3],:), expSigma([1,2],[1,2],:), [0 0 .8], 2);
%pause
%close all

%Vel = [0;0];
%Acc = [0;0];
%Anorm = [0];
%
%for i=2:N
%	V = (Data(:,i) - Data(:,i-1))/(Time(i)-Time(i-1));	
%	Vel = [Vel V];
%	Tang = V/sqrt(V'*V);
%	A = (Vel(:,i) - Vel(:,i-1))/(Time(i)-Time(i-1));	
%	Acc = [Acc A];
%	Anorm = [Anorm sqrt((A-(A'*Tang)*Tang)'*(A-(A'*Tang)*Tang))];
%end
%
%subplot(1,2,1)
%plot(Data(1,:),Data(2,:),'.');
%subplot(1,2,2)
%plot(Time,Anorm,'.');
%Anorm
%%GMR

end

function winDown(h,evt)
	setappdata(gcf,'getData',1);
	set(h,'WindowButtonMotionFcn',@winMove);
	set(h,'WindowButtonUpFcn',@winUp);
	if strcmp(get(gcf,'SelectionType'),'alt') == 1
		setappdata(gcf,'exitFlag',1);
		return
	end
	tic
end


function winUp(h,evt)
	setappdata(gcf,'getData',0);
end

function winMove(h,evt)
end

function lambda = EvalLambda(P)

	lambda = 1/10*(P'*P + 0.1);

end
