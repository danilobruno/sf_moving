function [ output_args ] = mouseData_time( input_args )
%MOUSEDATA Summary of this function goes here
%   Detailed explanation goes here

Time = [];
Data = [];

nbVar = 3;
exitFlag = 0;
scrollCounts = 0;

m = StiffFlop(3);
Tip = m.P(:,end);

L0 = m.L0;

fig = figure('position',[10 170 550 550]); hold on; box on;
plot3(-0.1,0,-0.1,'.');
plot3(0.1,0,0.1,'.');
view([0 -1 0]);
axis([-0.2 0.2 -0.1 0.1 -0.2 0.3]);
axis manual
axis equal
hhh = m.plot;
ss = linspace(0,2*pi,100);
trocar = [0.03*cos(ss);0.03*sin(ss);zeros(1,100)];
plot3(trocar(1,:),trocar(2,:),trocar(3,:),'Linewidth',3);hold on
%ObstMu = [0.1 -0.1 ;0 0 ;0.15 0.18];
%ObstSigma(:,:,1) = [0.0008 0 0.0006; 0 0.0004 0 ; 0.0006 0 0.001];
%ObstSigma(:,:,2) = [0.004 0 -0.0000; 0 0.0004 0 ; -0.0000 0 0.0004];
ObstMu = [0; 0 ; 0.15];
ObstSigma = [0.0005 0 0 ; 0 0.002 0 ; 0 0 0.0005]; 
plotGMM3D(ObstMu,ObstSigma,[0.2 0 0],1);
getData = 0;

set(0,'CurrentFigure',fig);

set(fig,'WindowButtonDownFcn',@winDown);
set(fig,'KeyPressFcn',@readkeys);
setappdata(gcf,'getData',getData);
setappdata(gcf,'exitFlag',exitFlag);
setappdata(gcf,'scrollCounts',scrollCounts);
setappdata(gcf,'Tip',Tip);
%Initialize DP-Kmeans
%lambda = 0.03;
%sigma = lambda*eye(3);

lambda = m.L0*1.8;
minsigma = lambda/300;

N = 0;
Priors = ones(1,3)/3;
Mu(:,1) = [-0.5*L0;m.getCoords(0.5)];
Mu(:,2) = [-1.5*L0;m.getCoords(1.5)];
Mu(:,3) = [-2.5*L0;m.getCoords(2.5)];
%%Build cloud of points around rest manipulator and get GMM
for i=1:3
	Sigma(:,:,i) = lambda*eye(4);
end
hhh1 = [];
hhh2 = [];
Ref = m.Refs(:,:,end);
P_old = 0;
go = 0;
step = 0.002;

setappdata(gcf,'step',step);
%t = toc;
%point = [2,1];
point = [1];
for ii = 1:length(point)
	P1(:,ii) = m.getCoords(point(ii));
end

while exitFlag == 0
	figure(fig)
	go = 0;
	%while getData == 1
		figure(fig); %hold off;
		%axis([-0.2 0.2 -0.1 0.3 -0.1 0.3]);
		%view([0 -1 0]);
		%point = get(gca,'CurrentPoint');
		setappdata(gcf,'Ref',Ref);
		getData = getappdata(gcf,'getData');
		Tip = getappdata(gcf,'Tip');
		Tip(2) = 0;
		%P = point(1,[1:3])';
		%if go == 1;
		%	P(2) = 0;
		%	Tip = 0.3*(P - P_old)+Tip;
		%	Tip(2) = 0;
		%end
		%P_old = P;
		%P_old(2) = 0;
		%go = 1;
		scrollCounts = getappdata(gcf,'scrollCounts');
		theta = scrollCounts*0.1;
		direction = [sin(theta),0,cos(theta)]';
		setappdata(gcf,'scrollCounts',0);
		%m.translateOf(Tip-m.P(:,end),[1 1 1 1 1],10);
		%m.rotateTo([sin(theta),0,cos(theta)]',[1 1 1 1 1],1);
		nullMotion = zeros(m.nbJVar,1);
		%%Evaluate best null motion with rbbc
		%
%%		%TEST
		%Trocar port control
		%
		modules = m.Links;
		discret= 10;
		zz = zeros(1,modules*discret);
		for i=1:modules*discret
			PPP = m.getCoords(i/discret);
			zz(i) = PPP(3);
		end
		[~,index] = min(zz<0);
		fraction = index/discret;
%		PPP = m.getCoords(fraction);
%		V = zeros(3,1);
%		J = m.jacobian;
%		JJTrocar = m.JJ(fraction);
%		if fraction > 0.1 
%			V([1,2],1) = -PPP([1,2],1); %Distance of point from trocar
%		else
%			V(:,1) = -PPP(:,1); %Distance of point from trocar
%			Mask = [2 0 .5 1 3];
%		end
%		nullMotion = pinv(JJTrocar)*V;
		%
		weight = [1 1];
		weight = [weight ones(1,modules)];
		[nullMotion,Target,weight] = rbbc1(point,Priors,Mu,Sigma,m,weight);
		%[nullMotion,Target,weight,t,P1] = rbbcDamp(t,P1,point,Priors,Mu,Sigma,m,weight);
		
		%nullMotion = rbbc(Priors,Mu,Sigma,m,weight);
		%point = [2,1];

%		for i=m.Links:-1:1
%			dd) && (Tip(3)>0)(i)  = sum(ddd([i:m.Links]));
%			VVV(:,i) = dd(i) * VVV(:,i);
%			nullMotion = nullMotion + pinv(m.JJ(i-1))*VVV(:,i);
%		end

%		%nullMotion = nullMotionLength + rbbc(Priors,Mu,Sigma,m);

		%nullMotion = nullMotionLength + nullMotion;

		%BaseLength = 3*(m.L0+2*m.d0);
		
		%weights = WeightEval(TotalLength,BaseLength);
%		if fraction > 2
%			weight = [1 1 0 0 0];
%		elseif fraction > 1 
%			weight = [1 1 1 1 0];
%		else
%			weight = [1 1 1 1 1];
%		end
		%point = [0,0.5,1,1.5,2,2.5];
		%nullMotionCheck = (eye(m.nbJVar) - pinv(m.jacobian)*m.jacobian)*nullMotion;
		%P_Check = m.getCoords(point);
		%V_Check = m.JJ(point)*nullMotionCheck;
		%for i=1:length(point)
		%	P_Check(:,i) = m.getCoords(point(i));
		%	V_Check(:,i) = m.JJ(point(i))*nullMotionCheck;
		%end
		%weight = [1 1 1 1 1];
		%weight = [1 4 0.3 0.3 0.3];
		%m.basicMotion(Tip,direction,nullMotion,weight,1);

		LLL = m.getConstCurvCoords(m.q);
		%TotalLength = sum(LLL) + m.Links*m.d0;  
		[~,L1] = m.getCoords(modules);
		[~,L2] = m.getCoords(fraction);
		TotalLength = L1 - L2;
		y = [TotalLength;Tip];
		nullMotionLength = zeros(m.nbJVar,1);
		for i=1:m.Links
			ddd(i) = LLL(i) - (m.L0)*(1+m.MaximumElong/2);
			VVV(:,i) = ddd(i) * m.Refs(:,3,i);
			nullMotionLength = nullMotionLength + pinv(m.JJ(i-1))*VVV(:,i);
		end
		nullMotion = 1/2*(nullMotion + nullMotionLength);
		%nullMotion = zeros(1,m.nbJVar)';
		m.basicMotion(Tip,direction,nullMotion,weight,10);
		%m.basicMotion(Tip,[0 0 1]',nullMotionLength,weight,5);
		%direction = [0 0 1]';
		%for ii=1:size(nullMotion,2)
		%	m.basicMotion(Tip,[0;0;1],nullMotion(:,ii),weight,1);
		%end

		delete(hhh);
		delete(hhh1);
		%delete(hhh2);
		hhh = m.plot;
		%y = [toc;P];
		%Data = [Data y];
		%plot(Data(2,:),Data(3,:),'k.','markerSize',2);hold on;
		%N = size(Data,2);
		%DP Kmeans with Data
		%Initialize at first point
		[Priors,Mu,Sigma,N] = OnlineEMDP(N,y,minsigma,Priors,Mu,Sigma,lambda);
		model.Mu = Mu;
		model.Sigma = Sigma;
		model.Priors = Priors;
		%rGMR = GMR(TotalLength,1,[2:4],model);
		%Att = rGMR.Mu;
		%hhh2 = plot3(P_Check(1,:),P_Check(2,:),P_Check(3,:),'o');
		%hhh2 = quiver3(P_Check(1,:),P_Check(2,:),P_Check(3,:),V_Check(1,:),V_Check(2,:),V_Check(3,:));
		
		tt = linspace(-3,TotalLength,100);
		rGMR = GMR(tt,1,[2:4],model);
		TrajAtt = rGMR.Mu;
		hhh1 = plot3(TrajAtt(1,:),TrajAtt(2,:),TrajAtt(3,:),'Linewidth',2,'color',[1 0.4 0.4]);
		
		Ref = m.Refs(:,:,end);

		%hhh1 = plotGMM3D(Mu([1:3],:),Sigma([1:3],[1:3],:),[1 0 0],0.3);

	%end
	getData = getappdata(gcf,'getData');
	exitFlag = getappdata(gcf,'exitFlag');
end


hhh1 = plotGMM3D(Mu([2:4],:),Sigma([2:4],[2:4],:),[1 0 0],0.3);
pause

close all

%nbData = N;
%nbStates = K;
%
%%% P(X|t) GMR
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%in=[1]; out=[2:3];
%expData(in,:) = linspace(min(Data(1,:)), max(Data(1,:)), nbData);
%expData(out,:) = zeros(length(out),nbData);
%expSigma = zeros(length(out),length(out),nbData);
%for n=1:nbData
%  %Weight
%  for i=1:nbStates
%    h(i) = gaussPDF(expData(in,n), Mu(in,i), Sigma(in,in,i));
%  end
%  h = h/sum(h);
%  for i=1:nbStates
%    %Compute expected conditional means
%    expData(out,n) = expData(out,n) + h(i) .* (Mu(out,i) + Sigma(out,in,i)*inv(Sigma(in,in,i)) * (expData(in,n)-Mu(in,i)));
%    %Compute expected conditional covariance matrices
%    expSigma(:,:,n) = expSigma(:,:,n) + h(i)^2 .* (Sigma(out,out,i) - (Sigma(out,in,i)*inv(Sigma(in,in,i))*Sigma(in,out,i)));
%  end
%end
%%GMR
%%
%figure()
%hold on;
%plotGMM(expData([2,3],:), expSigma([1,2],[1,2],:), [0 0 .8], 2);
%pause
%close all

%Vel = [0;0];
%Acc = [0;0];
%Anorm = [0];
%
%for i=2:N
%	V = (Data(:,i) - Data(:,i-1))/(Time(i)-Time(i-1));	
%	Vel = [Vel V];
%	Tang = V/sqrt(V'*V);
%	A = (Vel(:,i) - Vel(:,i-1))/(Time(i)-Time(i-1));	
%	Acc = [Acc A];
%	Anorm = [Anorm sqrt((A-(A'*Tang)*Tang)'*(A-(A'*Tang)*Tang))];
%end
%
%subplot(1,2,1)
%plot(Data(1,:),Data(2,:),'.');
%subplot(1,2,2)
%plot(Time,Anorm,'.');
%Anorm
%%GMR

end

function winDown(h,evt)
	setappdata(gcf,'getData',1);
	set(h,'WindowButtonMotionFcn',@winMove);
	set(h,'WindowButtonUpFcn',@winUp);
	set(h,'WindowScrollWheelFcn',@winWheel);
	if strcmp(get(gcf,'SelectionType'),'alt') == 1
		setappdata(gcf,'exitFlag',1);
		return
	end
	tic
end


function winUp(h,evt)
	setappdata(gcf,'getData',0);
end

function winWheel(h,evt)
	setappdata(gcf,'scrollCounts',evt.VerticalScrollCount);
end

function winMove(h,evt)
end

function readkeys(src,event)
	Tip = getappdata(gcf,'Tip');
	Ref = getappdata(gcf,'Ref');
	step = getappdata(gcf,'step');
	scrollCount = 0;
	if strcmp(event.Key,'uparrow') == 1
		Tip = Tip + step * Ref(:,3);
	elseif strcmp(event.Key,'downarrow') == 1
		Tip = Tip - step * Ref(:,3);
	elseif strcmp(event.Key,'leftarrow') == 1
		Tip = Tip - [step;0;0];
	elseif strcmp(event.Key,'rightarrow') == 1
		Tip = Tip + [step;0;0];
	elseif strcmp(event.Key,'z') == 1
		scrollCount = -1;
	elseif strcmp(event.Key,'x') == 1
		scrollCount = 1;
	elseif strcmp(event.Key,'q') == 1
		step = step + 0.001;
	elseif strcmp(event.Key,'a') == 1
		step = step - 0.001;
		if step == 0 
			step = 0.001;
		end
	end
	setappdata(gcf,'Tip',Tip);
	setappdata(gcf,'scrollCounts',scrollCount);
	setappdata(gcf,'step',step);
end

function lambda = EvalLambda(P)

	lambda = 1/10*(P'*P + 0.1);

end
