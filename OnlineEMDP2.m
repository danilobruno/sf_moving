function [ Priors,Mu,Sigma,N ] = OnlineEMDP2( N,P,MinSigma,Priors,Mu,Sigma,lambda )
%ONLINEEM Summary of this function goes here
%   Detailed explanation goes here
%Changed priors

if N == 0
	nbVar = size(P,1);
	Mu = P;
	Sigma = MinSigma*eye(nbVar);
	Priors = 1;
	N = 1;
	sp = 0.0001;
else
	nbVar = size(Mu,1);
	nbStates = size(Sigma,3);
	N = N+1;
	
	d = zeros(1,nbStates+1);
	for k=1:nbStates
		d(k) = sqrt((P-Mu(:,k))'*(P-Mu(:,k)));
	end
	d(nbStates+1) = lambda;
	[~,idx] = min(d);
	if idx == nbStates + 1
		nbStates = nbStates + 1;
		Mu(:,nbStates) = P;
		Sigma(:,:,nbStates) = MinSigma*eye(nbVar);
		Priors(nbStates) = 1/nbStates;
	else
		PriorsTmp = 1/N + Priors(idx);
		MuTmp = 1/PriorsTmp*(Priors(idx)*Mu(:,idx)+P/N);
		Sigma(:,:,idx) = Priors(idx)/PriorsTmp*(Sigma(:,:,idx)+(Mu(:,idx)-MuTmp)*(Mu(:,idx)-MuTmp)') + 1/(N*PriorsTmp)*(MinSigma*eye(nbVar)+(P-MuTmp)*(P-MuTmp)');
		Mu(:,idx) = MuTmp;
		%Priors(idx) = PriorsTmp;
		Priors(idx) = 1/nbStates;
	end
	Priors = Priors/sum(Priors);
end

end

