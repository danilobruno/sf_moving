function [ Priors,Mu,Sigma ] = initGMM( m )
%INITGMM Summary of this function goes here
%   Detailed explanation goes here

N = 1000;
modules = 3;
ss = linspace(0,modules,N);
[~,TotalLength] = m.getCoords(modules); 
for i=1:N
	[PTmp(:,i),LengthTmp(:,i)] = m.getCoords(ss(i));
	%PTmp(1,i) = PTmp(1,i) + 0.05*randn(1,1);
end
%DataInit(1,:) = - repmat(TotalLength,1,size(TotalLength,2)) + LengthTmp;
DataInit(1,:) = LengthTmp;
DataInit([2:4],:) = PTmp;
[Priors,Mu,Sigma] = EM_init_regularTiming(DataInit,modules,m.d0/100);
[Priors,Mu,Sigma] = EM(DataInit,Priors,Mu,Sigma,m.d0/100);

end

